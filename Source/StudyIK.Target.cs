// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class StudyIKTarget : TargetRules
{
	public StudyIKTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
        ExtraModuleNames.AddRange(new string[]
        {
            "StudyIK",
            "SSH_HelperModule"
        });
    }
}
