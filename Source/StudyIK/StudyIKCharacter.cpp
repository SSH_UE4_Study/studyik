// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "StudyIKCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Engine/CollisionProfile.h"
#include "Public/IKAnimInstanceBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../SSH_helperModule/Public/SSH_CollisionProfileHelper.h"

//////////////////////////////////////////////////////////////////////////
// AStudyIKCharacter

AStudyIKCharacter::AStudyIKCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AStudyIKCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AStudyIKCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AStudyIKCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AStudyIKCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AStudyIKCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AStudyIKCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AStudyIKCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AStudyIKCharacter::OnResetVR);

	PlayerInputComponent->BindKey(EKeys::L, EInputEvent::IE_Pressed, this, &AStudyIKCharacter::IKLog);
}

void AStudyIKCharacter::BeginPlay()
{
	Super::BeginPlay();

	IKCapsuleHalfHeight = GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
}

void AStudyIKCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	DeltaTickSeconds = DeltaSeconds;

// 	if (GetVelocity().Size() == 0.0f)
// 	{
// 		UpdateIK();
// 	}
// 	else
// 	{
// 		IKResetVal();
// 	}
}

void AStudyIKCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AStudyIKCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AStudyIKCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{

		StopJumping();
}

void AStudyIKCharacter::IKLog()
{
	FString LogString;

	LogString = FString::Printf(TEXT("IKLeftFootOffset : %f / IKRightFootOffset : %f / IKHipOffset : %f"), IKLeftFootOffset, IKRightFootOffset, IKHipOffset);

	UKismetSystemLibrary::PrintString(GetWorld(), LogString);
}

void AStudyIKCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AStudyIKCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AStudyIKCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AStudyIKCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AStudyIKCharacter::TestCharBlueprintNativeEventFunc_Implementation()
{

}

void AStudyIKCharacter::UpdateIK()
{
	UIKAnimInstanceBase* IKAnimInstanceBase = Cast<UIKAnimInstanceBase>(GetMesh()->GetAnimInstance());

	if (!IsValid(IKAnimInstanceBase))
		return;

	//좌측 발 IK트래이싱
	FVector LeftFootImpactNormal;
	IKFootTrace(IKTraceDist, IKLeftFootSocketName, IKLeftFootOffset, LeftFootImpactNormal);
	IKFootRotation(IKAnimInstanceBase->LeftFootRotation, NormalToRotation(LeftFootImpactNormal));

	// 우측 발 IK트래이싱
	FVector RightFootImpactNormal;
	IKFootTrace(IKTraceDist, IKRightFootSocketName, IKRightFootOffset, RightFootImpactNormal);
	IKFootRotation(IKAnimInstanceBase->RightFootRotation, NormalToRotation(RightFootImpactNormal));

	// 트래이싱 오프셋이 캡슐바닥보다 아래쪽에 위치한경우 스켈탈 메시의 최상단 본의 위치를 조정해서 캐릭터와 바닥간의 높이차를 제거한다.
	if (FMath::Min<float>(IKLeftFootOffset, IKRightFootOffset) < 0.0f)
	{
		IKHipOffset = FMath::Min<float>(IKLeftFootOffset, IKRightFootOffset);

		IKUpdateOffset(IKAnimInstanceBase->HipOffset, IKHipOffset);
	}
	else
	{

		IKHipOffset = 0.0f;
	}

	// 조정한 hipoffset만큼의 오차수정만큼 좌우 발의ik effect 높이 처리를 한다.
	IKUpdateOffset(IKAnimInstanceBase->LeftEffecOffset, (IKLeftFootOffset - IKHipOffset));
	IKUpdateOffset(IKAnimInstanceBase->RightEffectOffset, (IKRightFootOffset - IKHipOffset));
}

void AStudyIKCharacter::IKFootTrace(float TraceDist, FName TraceSocketName, float& OutTraceOffset, FVector& OutImpactNormal)
{
	OutTraceOffset = 0.0f;
	OutImpactNormal = FVector::ZeroVector;

	USkeletalMeshComponent* SkeletalMeshComp = GetMesh();
	if (IsValid(SkeletalMeshComp))
	{
		FVector SocketLoc = SkeletalMeshComp->GetSocketLocation(TraceSocketName);
		FVector ActorLoc = GetActorLocation();

		const UCollisionProfile* CollisionProfile = UCollisionProfile::Get();
		if (IsValid(CollisionProfile))
		{
// 			ECollisionChannel CollisionChannel;
// 			FCollisionResponseParams CollisionResponseParams;
// 
// 			CollisionProfile->GetChannelAndResponseParams(FName("Visibility"), CollisionChannel, CollisionResponseParams);
// 			ETraceTypeQuery TraceTypeQuery = CollisionProfile->ConvertToTraceType(CollisionChannel);

			ETraceTypeQuery TraceTypeQuery = USSH_CollisionProfileHelper::GetTraceQueryByTraceName(FName("Visibility"));

			TArray<AActor*> TraceIgnore = { this };
			FHitResult HitResult;

			// 스켈레탈 메시의 소캣을 기준으로 바닥에 레이케스팅 테스트
			bool TraceSuccess = UKismetSystemLibrary::LineTraceSingle(
									GetWorld(),
									FVector(SocketLoc.X, SocketLoc.Y, ActorLoc.Z),
									FVector(SocketLoc.X, SocketLoc.Y, (ActorLoc.Z - IKCapsuleHalfHeight - TraceDist)),
									TraceTypeQuery,
									true,
									TraceIgnore,
									EDrawDebugTrace::None,
//									EDrawDebugTrace::ForOneFrame,
									HitResult,
									true
								);

			if (TraceSuccess)
			{
				if (HitResult.bBlockingHit)
				{
					// 충돌지점과 캐릭터 켑슐의 바닥을 기준으로 IK트레이싱 포인트가 위쪽(-)인지 아래쪽(+)인지 오프셋처리
					OutTraceOffset = (HitResult.Location - HitResult.TraceEnd).Size() - TraceDist + IKAdjustOffset;
				}
				else
				{
					OutTraceOffset = 0.0f;
				}

				OutImpactNormal = HitResult.ImpactNormal;
			}
		}
	}
}

void AStudyIKCharacter::IKFootRotation(FRotator& MeshFootRotator, FRotator TargetRotator)
{
	MeshFootRotator = FMath::RInterpTo(MeshFootRotator, TargetRotator, DeltaTickSeconds, 7);
}

void AStudyIKCharacter::IKUpdateOffset(float& EffectorVal, float EffectTarget)
{
	EffectorVal = FMath::FInterpTo(EffectorVal, EffectTarget, DeltaTickSeconds, 7);
}

void AStudyIKCharacter::IKResetVal()
{
	UIKAnimInstanceBase* IKAnimInstanceBase = Cast<UIKAnimInstanceBase>(GetMesh()->GetAnimInstance());

	if (!IsValid(IKAnimInstanceBase))
		return;

	IKUpdateOffset(IKAnimInstanceBase->LeftEffecOffset, 0.0f);
	IKFootRotation(IKAnimInstanceBase->LeftFootRotation, FRotator::ZeroRotator);
	IKUpdateOffset(IKAnimInstanceBase->RightEffectOffset, 0.0f);
	IKFootRotation(IKAnimInstanceBase->RightFootRotation, FRotator::ZeroRotator);
	IKUpdateOffset(IKAnimInstanceBase->HipOffset, 0.0f);
}

FRotator AStudyIKCharacter::NormalToRotation(FVector Normal)
{
	FRotator RetRotator;

	RetRotator.Roll = UKismetMathLibrary::DegAtan2(Normal.Y, Normal.Z);
	RetRotator.Pitch = UKismetMathLibrary::DegAtan2(Normal.X, Normal.Z);
	RetRotator.Yaw = 0.0f;

	return RetRotator;
}

